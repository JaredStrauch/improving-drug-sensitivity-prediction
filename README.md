# Improving Anticancer Drug Sensitivity Prediction

These files will replicate the results from "Improving Drug Sensitivity Prediction and Inference by Multitask Learning." To do, set the directory you would like to read data from and save the results to using the 00-paths file. Follow the directions in 30-replication to download the data and process it into the feature and response matrices. After assigning directories and downloading the data everything else should not require any effort from you, except for running the programs in sequential order. File 31 fixes data leakage from an experiment in the paper from which we get our data. File 32 creates plots to visualize the differences in results with and without data leakage. File 33 implements the Data Shared Elastic Net. File 34 creates the plots comparing the predicition accuracy from the Data Shared Elastic Net against Elastic Net regression.


