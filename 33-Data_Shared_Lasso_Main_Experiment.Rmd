---
title: "33- Data Shared Elastic Net Main Experiment"
author: "Jared Strauch"
date: "6/6/2023"
output: 
  html_document:
    toc: true
    theme: yeti
    highlight: kate
    number_sections: true
---

```{r}
rm(list=ls())
source("00-paths.R")
require(caret)
require(e1071)
require(Matrix)
require(glmnet)
require(RColorBrewer)
```

```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
options(width=96)
```

```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```


# Executive Summary
## Background
We want to tune the hyperparameters via cross-validation for the Data Shared Elastic Net (DSEN) models and then perform 200 bootstrap resamples of our data, refitting the model with our previously selected hyperparameters to generate measures of feature importance. Our two measures of feature importance are:
1. Average coefficient estimates over the bootstrap resamples.
2. The proportion of times each shared or tissue-specific coefficient estimate is non-zero for each feature.
We are following the same basic procedure as we did when replicating Ghandi et al. 2019.


## Methods
### Input Data
We have preprocessed the data in file 30 and will use that.

### Procedure 
1. Load the data.
2. Assemble the data in the manner as described in Gross and Tibshirani 2016.
3. Perform 10-fold cross-validation to tune our hyperparameters.
4. Perform 200 bootstrap resamples and fit our models.
5. Plot a heatmap of our mean estimated coefficients and proportion of times each feature is non-zer0.

### Output Data 
We save a list of hyperparameters that minimize our cv error, the range of hyperparameters used, the mean cv error and standard deviation of our cv error. A list of our results from the bootstrap are saved as well as a heatmap that is saved as a PDF.

Functions from Gross and Tibsharani 2016
```{r}
# This version lets you fit just one of the CV models done by cv.del1
del1.glmnet = function(datalist, lamrat, lam, thr=1e-12, w=NULL, ...){
	nsets = length(datalist)
	p = ncol(datalist[[1]]$x)
	ns = sapply(datalist, function(item){nrow(item$x)})
	
	# MAYBE ADD SOME CHECKS HERE TO MAKE SURE
	# THAT THE DATASETS ARE COMPATABLE OR ELSE ERR	
	
	# make Z
	cns = cumsum(ns)
	startrows = c(1,cns[-length(cns)] + 1)
	endrows = cns
	Z = Matrix(0, cns[length(cns)], p + nsets*p)
	for(ii in 1:nsets){
		# first cols
		Z[startrows[ii]:endrows[ii], 1:p] = 
		                           datalist[[ii]]$x
	    # other cols
	    Z[startrows[ii]:endrows[ii], 1:p + p*ii] = 
	                               datalist[[ii]]$x
	}
	
	yz = unlist(lapply(datalist, function(item){item$y}))
	
	if(is.null(lamrat)){
		pf = rep(1, (nsets+1)*p)
		myexclude = (p+1):((nsets+1)*p)
	}
	else{
		pf = rep(c(lamrat, 1), 
	         times = c(p,nsets*p))
	    myexclude = integer(0)
	}
	
	if(is.null(w)){w = rep(1,cns[length(cns)])}
	glmnet(Z, yz, thresh=thr, 
	          weights=w, penalty.factor=pf, 
	          intercept=FALSE, standardize = FALSE, 
	          exclude = myexclude, lambda=lam,...)
}



getj = function(mat){
	dp = diff(mat@p)
	rep(seq_along(dp), dp) - 1
}

del1.spar = function(datalist, lam1, lam2, thr=1e-12, w=NULL){
	nsets = length(datalist)
	p = ncol(datalist[[1]]$x)
	ns = sapply(datalist, function(item){nrow(item$x)})
	
	# MAYBE ADD SOME CHECKS HERE TO MAKE SURE
	# THAT THE DATASETS ARE COMPATABLE OR ELSE ERR	
	
	# make Z
	cns = cumsum(ns)
	startrows = c(1,cns[-length(cns)] + 1) - 1
	endrows = cns
	n = tail(cns,1)
	
	is = NULL
	js = NULL
	xs = NULL
	
	for(ii in 1:nsets){
		# first cols
		is = c(is, startrows[ii] + datalist[[ii]]$x@i)
		js = c(js, getj(datalist[[ii]]$x))
		xs = c(xs, datalist[[ii]]$x@x)
		
		# 2nd copy
		is = c(is, startrows[ii] + datalist[[ii]]$x@i)
		js = c(js, getj(datalist[[ii]]$x) + p*ii)
		xs = c(xs, datalist[[ii]]$x@x)
	}
	Z = sparseMatrix(i = is, j = js, x = xs, index1=FALSE, dims = c(n,p*(nsets+1)))
	
	yz = unlist(lapply(datalist, function(item){item$y}))
	
	lambda = (lam1 + lam2)
	pf = rep(c(lam1/lambda, lam2/lambda), 
	         times = c(p,nsets*p))
	#print(length(yz))
	#print(pf*lambda)
	
	print(dim(Z))
	
	las.glmnet(Z,yz,lambda,thresh=thr,w=w,pf=pf)
}



#lamrat = lam1/lam2.  Setting lamrat > nsets results in completely seperate lassos.  lamrat = NULL will set lam2 = INF (equivalent to one pooled lasso)

del1.cv.spar = function(datalist, lamrat = NULL, thr=1e-8, w=NULL, ...){
	nsets = length(datalist)
	p = ncol(datalist[[1]]$x)
	ns = sapply(datalist, function(item){nrow(item$x)})
	
	# MAYBE ADD SOME CHECKS HERE TO MAKE SURE
	# THAT THE DATASETS ARE COMPATABLE OR ELSE ERR	
	
	# make Z
	cns = cumsum(ns)
	startrows = c(1,cns[-length(cns)] + 1) - 1
	endrows = cns
	n = tail(cns,1)
	
	is = NULL
	js = NULL
	xs = NULL
	
	for(ii in 1:nsets){
		# first cols
		is = c(is, startrows[ii] + datalist[[ii]]$x@i)
		js = c(js, getj(datalist[[ii]]$x))
		xs = c(xs, datalist[[ii]]$x@x)
		
		
		# 2nd copy
		is = c(is, startrows[ii] + datalist[[ii]]$x@i)
		js = c(js, getj(datalist[[ii]]$x) + p*ii)
		xs = c(xs, datalist[[ii]]$x@x)
	}
	Z = sparseMatrix(i = is, j = js, x = xs, index1=FALSE, dims = c(n,p*(nsets+1)))
	
	print(dim(Z))
	
	yz = unlist(lapply(datalist, function(item){item$y}))
	
	if(is.null(lamrat)){
		pf = rep(1, (nsets+1)*p)
		myexclude = (p+1):((nsets+1)*p)
	}
	else{
		pf = rep(c(lamrat, 1), 
	         times = c(p,nsets*p))
	    myexclude = integer(0)
	}
	
	if(is.null(w)){w = rep(1,cns[length(cns)])}
	cv.glmnet(Z, yz, thresh=thr, 
	          weights=w, penalty.factor=pf, 
	          intercept=T, standardize = F, 
	          exclude = myexclude,...)
}

predict.del1= function(datalist, longcoefs){
	p = length(longcoefs)/(length(datalist)+1)
	mypreds = unlist(lapply(datalist, function(item){
		as.numeric(item$x %*% longcoefs[1:p])
	}))
	mypreds = mypreds + 
	          unlist(lapply(seq(length(datalist)),
	                        function(ii){
	                          as.numeric(datalist[[ii]]$x %*% 
	                          longcoefs[1:p + (ii)*p])
	                        }
	                        ))
	mypreds
}
```

Neither of the del1.glmnet nor del.spar functions work so I'm going to patchwork things together, this is the function to use if fitting one model.
```{r}
del1.spar.test = function(datalist, lam, lamrat, alpha, thr=1e-8, w=NULL){
	nsets = length(datalist)
	p = ncol(datalist[[1]]$x)
	ns = sapply(datalist, function(item){nrow(item$x)})
	
	# MAYBE ADD SOME CHECKS HERE TO MAKE SURE
	# THAT THE DATASETS ARE COMPATABLE OR ELSE ERR	
	
	# make Z
	cns = cumsum(ns)
	startrows = c(1,cns[-length(cns)] + 1) - 1
	endrows = cns
	n = tail(cns,1)
	
	is = NULL
	js = NULL
	xs = NULL
	
	for(ii in 1:nsets){
		# first cols
		is = c(is, startrows[ii] + datalist[[ii]]$x@i)
		js = c(js, getj(datalist[[ii]]$x))
		xs = c(xs, datalist[[ii]]$x@x)
		
		# 2nd copy
		is = c(is, startrows[ii] + datalist[[ii]]$x@i)
		js = c(js, getj(datalist[[ii]]$x) + p*ii)
		xs = c(xs, datalist[[ii]]$x@x)
	}
	Z = sparseMatrix(i = is, j = js, x = xs, index1=FALSE, dims = c(n,p*(nsets+1)))
	
		print(dim(Z))
		
	yz = unlist(lapply(datalist, function(item){item$y}))
	
	if(is.null(lamrat)){
		pf = rep(1, (nsets+1)*p)
		myexclude = (p+1):((nsets+1)*p)
	}
	else{
		pf = rep(c(lamrat, 1), 
	         times = c(p,nsets*p))
	    myexclude = integer(0)
	}
	
	if(is.null(w)){w = rep(1,cns[length(cns)])}
	glmnet(Z, yz, thresh=thr, 
	          weights=w, penalty.factor=pf, 
	          intercept=T, standardize = FALSE, 
	          exclude = myexclude, lambda=lam, alpha = alpha)
}
```

Load Data
```{r}
load(file.path(paths$clean, 'Sanger.Rdata'))
load(file.path(paths$clean, 'ccle.Rda'))
```

```{r}
drugs <- c("Tanespimycin", "Saracatinib", "Selumetinib", "Erlotinib", "Lapatinib", "Nilotinib", "Nutlin-3a (-)", "Paclitaxel", "PD0325901", "Palbociclib", "Crizotinib", "PHA-665752", "PLX-4720", "Sorafenib", "NVP-TAE684")
not.run <- setdiff(colnames(Sanger$dat), drugs)
```


Use cross-validation to Tune Lambda and Alpha
```{r}
for (drug in drugs){

start.time <- Sys.time()
print(drug)

#The DSL functions want a list of features and responses by task, so we are going to parse out the data by tissue type
mydata <- list()
y <- Sanger$dat[,drug] #Grab drug response info
y <- as.data.frame(y)
y$tissue <- Sanger$tissue #add on tissue type 

y <- y[!is.na(y$y),] #remove cell lines without response data

#only keep rows with response and predictor measurements 
y <- y[which(rownames(y) %in% rownames(x)),]
pred <- x[which(rownames(x) %in% rownames(y)),] 

#make sure everything is in the right order
y <- y[order(rownames(y)),]
pred <- pred[order(rownames(pred)),]

#replace NAs with 0s
pred[is.na(pred)] = 0

#remove duplicate columns
pred = pred[, !duplicated(t(pred))]


#standardize excluding mutations, doesn't make sense to standardize binary variables
pred[,-grep('mut', colnames(pred))] <- scale(pred[,-grep('mut', colnames(pred))])

#loop across tissues 
for (tissue in unique(y$tissue)){
index <- which(y$tissue == tissue) #select index of cell lines for a tissue type
#if there are no cell lines within the tissue, stop here
if(length(index) > 0){

#response for that tissue type
y2 <- y[index,]

#save cell line names for later
names  <- rownames(y2)

#get rid of tissue type
y2 <- as.vector(y2[,"y"])

#Reassign cell line names from earlier
names(y2) <- names

#get features for the cell lines we selected earlier, if only one cell line it will turn into a vector which messes up the code later, so we need to make it a matrix.
pred2 <-if(length(index) == 1){matrix(pred[index,], nrow = 1)} else{pred[index,]}

#need to turn predictor matrix into a sparse matrix, which only stores non-zero values

#get index of non-zero values
x.0.ind <- which(pred2 != 0, arr.ind = T)

#create sparse matrix, i is the row index and j is the column index, and x is the non-zero responses. If only one column in pred2 it will output as a list which breaks the sparseMatrix function, so we use unlist
x.spar <- sparseMatrix(i = x.0.ind[,1], j = x.0.ind[,2], x = unlist(pred2[x.0.ind]))

#assign response and sparse feature matrix to the tissue type in our list
mydata[[tissue]] <- list(y = y2, x = x.spar)
  } 
}

#Now we're going to assemble the Z matrix and set up r_t values through the use of penalty factors in the cv.glmnet function. pf controls the amount of regularization with smaller values leading to less shrinkage. Setting a pf for the non block diagonal features is equivalent to the inverse of r_t, i.e. lamrat = 1/r_t

#recommended value from DSL paper
lamrat <- sqrt(length(mydata))

#number of tasks
nsets = length(mydata)

  #number of features
	p = ncol(mydata[[1]]$x)
	
	#number of observations in each tissue
	ns = sapply(mydata, function(item){nrow(item$x)})
	
	# MAYBE ADD SOME CHECKS HERE TO MAKE SURE
	# THAT THE DATASETS ARE COMPATABLE OR ELSE ERR	
	
	# make Z
	#cumulative # of observations
	cns = cumsum(ns)
	
	#Figure out rows so we can assign data
	startrows = c(1,cns[-length(cns)] + 1) - 1
	endrows = cns
	n = tail(cns,1)
	
	is = NULL
	js = NULL
	xs = NULL
	
	#assemble Z with row indexes from above, first cols is the normal feature matrix, 2nd copy are the block diagonal matrices.
	for(ii in 1:nsets){
		# first cols
		is = c(is, startrows[ii] + mydata[[ii]]$x@i)
		js = c(js, getj(mydata[[ii]]$x))
		xs = c(xs, mydata[[ii]]$x@x)
		
		
		# 2nd copy
		is = c(is, startrows[ii] + mydata[[ii]]$x@i)
		js = c(js, getj(mydata[[ii]]$x) + p*ii)
		xs = c(xs, mydata[[ii]]$x@x)
	}
	Z = sparseMatrix(i = is, j = js, x = xs, index1=FALSE, dims = c(n,p*(nsets+1)))
	
	print(dim(Z))
	
	#vector of responses
	yz = unlist(lapply(mydata, function(item){item$y}))
	
#set up pf
pf = rep(c(lamrat, 1), times = c(p,nsets*p))

#not excluding or weighting anything
myexclude = integer(0)
w = rep(1,cns[length(cns)])

set.seed(1337)

#alpha values for grid search
alphas <- seq(0.2, 1, 0.1)

#create place to store results
lambdas = vector("list", length(alphas))  #lambdas used
meanErr = vector("list", length(alphas))  #sum of errors
sdErr = vector("list", length(alphas))  #sum of errors
cvfit <- NULL

#do grid search
for (j in 1:length(alphas)) {
cvfit <-	cv.glmnet(Z, yz, alpha = alphas[j], thresh=1e-8, 
	          weights=w, penalty.factor=pf, 
	          intercept=T, 
	          exclude = myexclude, standardize = F)
       
    meanErr[[j]] = cvfit$cvm
    sdErr[[j]] = cvfit$cvsd
    lambdas[[j]] = cvfit$lambda   
}

#find minimum cv error and get corresponding alpha and lambda 
jmin = order(unlist(lapply(meanErr, min)))[1]
imin = order(meanErr[[jmin]])[1]
alpha.min = alphas[jmin]
lambda.min = lambdas[[jmin]][imin]

#save the results
params <- list("lambdas" = lambdas, "alphas" = alphas, "sdErr" = sdErr, "meanErr" = meanErr, "alpha.min" = alpha.min, "lambda.min" = lambda.min)
f <- paste0('DSEN_cv_', drug)
save(params, file =file.path(paths$scratch, paste0(f, '.Rda')))

end.time <- Sys.time()
print(end.time - start.time)
}
```

Bootstrap in the same way as Barretina 2012 and Ghandi 2019 CCLE papers.
```{r}
bootstrap <- function(lam, alpha, n=200){

#get parameters for resample
N = nrow(y); 

#cnt is used to record the number of times an estimated feature coefficient is non-zero in resamples
cnt = rep(0, ncol(pred)); names(cnt)=colnames(pred)

#sx is used to record the estimated feature coefficient in resamples
sx = Matrix(0, nrow = ncol(pred), ncol = length(unique(Sanger.2019[["clinfo"]][["GDSC.Tissue.descriptor.1"]]))); rownames(sx)=colnames(pred)
colnames(sx)= c("Total", unique(Sanger.2019[["clinfo"]][["GDSC.Tissue.descriptor.1"]])[-20])


#run the resamples
for(bsi in 1:n){
    
#resample
ii = sample(N, N, replace =TRUE)
bs.y <- y[ii,]
bs.pred <- pred[ii,]

mydata <- list()
#build list for model
for (tissue in unique(Sanger.2019[["clinfo"]][["GDSC.Tissue.descriptor.1"]])){
index <- which(bs.y$tissue == tissue)
if(length(index) > 0){

bs.y2 <- bs.y[index,]
names  <- rownames(bs.y2)

bs.y2 <- as.vector(bs.y2[,"y"])
names(bs.y2) <- names

bs.pred2 <- bs.pred[index,]

#need to turn predictor matrix into a sparse matrix

x.0.ind <- which(bs.pred2 != 0, arr.ind = T)

x.spar <- sparseMatrix(i = x.0.ind[,1], j = x.0.ind[,2], x = bs.pred2[bs.pred2 != 0])

mydata[[tissue]] <- list(y = bs.y2, x = x.spar)
#print(tissue)
  } 
}

#fit the DSEN
fit = del1.spar.test(mydata, lam, lamrat = sqrt(length(mydata)), alpha = alpha, thr=1e-12, w=NULL)
#cc = coef(fit,  s = lam) 
w = Matrix(coef(fit, s=lam)[-1], ncol = (length(mydata) +1)) #matrix with features as rows and shared and tissue specific estimates are columns, ignore intercept estimate

colnames(w) <- c("Total", names(mydata))

jj = which(abs(w)>0, arr.ind = T)
cnt[unique(jj[,1])] = cnt[unique(jj[,1])]+1; #add one to the count for predictors selected

for(i in colnames(w)){
 sx[,i] <-  sx[,i] + w[,i] #add estimate of the coefficient so that we can calculate the mean later, do it by column in case one of the tissues isn't included
}

print(bsi)
}


sx = sx/n; #mean estimated coefficient

cnt = cnt/n #proportion of times predictor is selected by the model
return(list(cnt = cnt, w = sx))
}
```


```{r}
#If you want to run hyperparameter tuning and bootstrap at once we can skip most of this, otherwise we have to reassemble our Z matrix and load in hyperparameters.
for (drug in drugs) {
start.time <- Sys.time()

mydata <- list()
y <- Sanger.2019$dat[,drug] #Grab drug response info
names(y) <- names(Sanger.2019$cls)
y <- as.data.frame(y)
y$tissue <- Sanger.2019[["clinfo"]][["GDSC.Tissue.descriptor.1"]]
#need to keep cell lines that we have data for and select by tissue type


y <- y[!is.na(y$y),]

#only keep rows with response and predictor measurements 
y <- y[which(rownames(y) %in% rownames(x2)),]
pred <- x2[which(rownames(x2) %in% rownames(y)),] 

#make sure everything is in the right order
y <- y[order(rownames(y)),]
pred <- pred[order(rownames(pred)),]

#Set NAs to 0
pred[is.na(pred)] = 0

#remove duplicate columns
pred <- pred[,!duplicated(t(pred))]

pred[,-grep('mut', colnames(pred))] <- scale(pred[,-grep('mut', colnames(pred))])

#load in hyperparameters
f <- paste0('DSEN_cv_', drug, ".Rda")
file.path <- paste0("/Volumes/Repository/Documents/Paths/scratch/", f)
load(file.path)

b <- bootstrap(lam = params$lambda.min, alpha = params$alpha.min)

end.time <- Sys.time()
print(drug)
print(end.time - start.time)

f <- paste0('DSEN_BS_', drug, ".RDA")
file.path <- paste0("/Volumes/Repository/Documents/Paths/scratch/", f)
save(b, file =file.path)

#plot results in a heatmap for coefficient estimates
mat <- as.matrix(b$w)
#only look at top 15 features by non-zero proportion
mat <- mat[order(-b$cnt)[1:15],]
rownames(mat) <- paste0(rownames(mat), "(", sort(b$cnt, decreasing = T)[1:15], ")")

#make names look better
colnames(mat) <- str_to_title(gsub("_", " ", colnames(mat)))
colnames(mat)[grep("Lung Sclc", colnames(mat))] <- "Lung SCLC"
colnames(mat)[grep("Lung Nsclc", colnames(mat))] <- "Lung NSCLC"

f <- paste0('BS_Heatmap_', drug)
outfn <- file.path(dir_name, f)

pdf(paste(outfn, '.pdf', sep = ''),
      width = 10,
      height = 10)

heatmap(mat, 
        col = colorRampPalette(c("blue", "white", "red"))(100), # Define color gradient
        main = drug,
        xlab = "Tissues",
        ylab = "Features",
        Rowv = NA,
        Colv = NA,
        cexCol = 1,
        cexRow = 0.65,
        mgp = c(3,1.5,0),
        margins = c(9,9),
        scale = "none",
        breaks = seq(from = -1*max(abs(mat)), to = max(abs(mat)), length.out = 101)
        )
par(new = T, mai=c(9.75, 8, 0.1, 0.1), mgp = c(3, 0.7, 0), xpd = F)
  image(seq(from = -1*max(abs(mat)), to = max(abs(mat)), length.out = 101), 1, as.matrix(seq(from = -1*max(abs(mat)), to = max(abs(mat)), length.out = 101)), col = colorRampPalette(c("blue", "white", "red"))(100), axes = F, ylab="", xlab="Coefficient Values", cex.lab = 0.7, mgp = c(1.5, 1, 0))
  axis(1, cex.axis = 0.7)
dev.off()
}
```

Plots used to compare DSEN and Elastic Net results are in file 34.