---
title: 'Data Preparation for Barrentina Replication'
author: "Amir Asiaee"
date: "30 January 2022"
output:
  html_document:
    toc: true
    theme: yeti
    highlight: kate
    number_sections: true
---
  
```{r setup, include=FALSE, results="hide"}
knitr::opts_chunk$set(echo = TRUE)
options(width=96)
```
```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

# Executive Summary
## Background
In this file we want to prepare and save the drug screening data of the Barrentina 2012 and 2019 paper. 

## Methods
### Input Data
We have got access to the exact code that has been used to generate the figures. They also included all of the required data to make the matrix of perdictors X in a Google Drive folder and included instructions on how to get the outcome Y (drug sensitivity results) from 

### Procedure 
The data, code, and instruction is downloaded from https://drive.google.com/drive/folders/15wAQDZ1yE7BD_mH34hNkpzp9K-37etxD
Under the `R` folder, there are subfolders for each figure of the paper where you can find the relevant codes. We are interested in `FigS11i_RPPA_elasticnet.R`. There you can find the subfolders that contain required data to make the matrix X. I have selected those files and put them in my database (that I access using `00-paths.R` through the corresponding JSON file.)

Then you need to search for `Sanger.R` for instructions to download GDSC data and make Y. I have simplifed that procedure and put it all here. 

### Output Data 
The ouput data are X and Y data files per each drug which we will use in the downstream analysis. 
We are saving everything in `ccle.Rda`: cellline, X, y. 

### Statistics

## Results

## Conclusion

# Basics
First we prepare the paths for clean/raw/scratch data:
```{r pats}
rm(list=ls())
source("00-paths.R")
```


## Building X

### Loading files
These can be found in the Google Drive folder and you need to copy them into your database folder under `clean` subfolder. 
```{r loadData}
load(file.path(paths$clean,'mutations_20181208.Rdata'))
load(file.path(paths$clean,'CN_20181002.RData'))
load(file.path(paths$clean,'Expr_20181002.RData'))
load(file.path(paths$clean,'RPPA_20181003.RData'))
load(file.path(paths$clean,'Annotations.RData'))
```

Now, like the 2019 paper we make two X matrices (with and without Protein expression data)

```{r , warning = FALSE}
x2 = cbind(muthrec$dat,
          muths$dat,
          mutdel$dat,
          mutmis$dat,
          Expr$dat,
          CN$dat)
x = cbind(muthrec$dat,
           muths$dat,
           mutdel$dat,
           mutmis$dat,
           Expr$dat,
           CN$dat,
           RPPA$dat)

colnames(x2) = c(
  paste0(muthrec$name, '_mut.hsrec'),
  paste0(muths$name, '_mut.hs'),
  paste0(mutdel$name, '_mut.tr'),
  paste0(mutmis$name, '_mut.mis'),
  paste0(Expr$name, '_Expr'),
  paste0(CN$name, '_CN')
)
colnames(x) = c(
  paste0(muthrec$name, '_mut.hsrec'),
  paste0(muths$name, '_mut.hs'),
  paste0(mutdel$name, '_mut.tr'),
  paste0(mutmis$name, '_mut.mis'),
  paste0(Expr$name, '_Expr'),
  paste0(CN$name, '_CN'),
  RPPA$name
)
```




## Building Y

### Downloading files
First download the following three files from GDSC ftp server (In Windows go to Windows Explorer -> ftp://ftp.sanger.ac.uk/pub4/cancerrxgene/releases/ and then click on `release-6.0` folder): 
  - Annotated list of Cell lines: `Cell_Lines_Details.csv`  
    - save the first page of the Cell_Lines_Details.xlsx as .csv file
  - Screened compounds: `Screened_Compounds.txt`  
    - save the first page of the Screened_Compounds.xlsx as **tab delimited** .txt file, replace "Drug ID" by "DRUG ID", "Drug Name" by "DRUG NAME", and "Target Pathway" by "TARGET PATHWAY" in the header row
  - log(IC50) and AUC values: `v17_fitted_dose_response.txt`
    -save the first page of the v17_fitted_dose_response.xlsx as **tab delimited** .txt file

Put all three CSV and TXT files into your database's clean folder. 

```{r loadData}
D = read.delim(file.path(paths$clean,'v17_fitted_dose_response.txt'), sep='\t', stringsAsFactors = FALSE)
sif = read.delim(file.path(paths$clean, 'Screened_Compounds.txt'), sep='\t', stringsAsFactors = FALSE) #tab delimited
clsif = read.csv(file.path(paths$clean, 'Cell_Lines_Details.csv'), stringsAsFactors = FALSE)
head(table(sif$DRUG.ID))
head(clsif)
head(D)
```


You need to run the below code chunck to save the required Y file: 

```{r , warning = FALSE}
f <- file.path(paths$clean, 'Sanger.Rdata')
if(file.exists(f)) {
  load(f)
} else {
  drug_ids = unique(D$DRUG_ID)
  ii = match(drug_ids, sif$DRUG.ID)
  
  info = sif[ii, ]
  Sanger = newDat(info$DRUG.ID)
  Sanger$info = info
  
  
  jj = match(D$DRUG_ID, drug_ids)
  ii = match(D$COSMIC_ID, clsif$COSMIC.identifier)
  clnam = CleanCellLineName(clsif$Sample.Name)[ii]
  Sanger$clinfo = clsif$GDSC.Tissue.descriptor.1[ii]
  ii = match(clnam, cellLineNames)
  
  hist(D$LN_IC50, 100)
  ic50 = Sanger$dat
  rmse = ic50
  
  max_conc = ic50
  
  kk = which(!is.na(ii))
  
  
  Sanger$dat[cbind(ii, jj)[kk, ]] = D$AUC[kk]
  ic50[cbind(ii, jj)[kk, ]] = D$LN_IC50[kk]
  rmse[cbind(ii, jj)[kk, ]] = D$RMSE[kk]
  max_conc[cbind(ii, jj)[kk, ]] = D$MAX_CONC_MICROMOLAR[kk]
  
  Sanger$ic50 = ic50
  Sanger$rmse = rmse
  Sanger$max_conc = max_conc
  
  #use drugname, remove dupl names
  dnam = Sanger$info$DRUG.NAME
  dnam2 = paste0(Sanger$info$DRUG.NAME, '_repl_', Sanger$info$DRUG.ID)
  ii = which(duplicated(dnam))
  dnam[ii] = dnam2[ii]
  Sanger$name = dnam
  colnames(Sanger$dat) = dnam
  
  #grab tissue type
  Sanger$tissue <- clsif$GDSC.Tissue.descriptor.1[match(names(Sanger$cls), CleanCellLineName(clsif$Sample.Name))]
  #fix duplicates
 Sanger$tissue[which(rownames(Sanger$dat) == "TT_THYROID")] <- "thyroid"
 Sanger$tissue[which(rownames(Sanger$dat) == "KMH2_THYROID")] <- "thyroid"
  Sanger$cls = rowSums(!is.na(Sanger$dat)) > 0
  table(Sanger$cls)
  save(Sanger, file = f)
}
```
There are 1462 celllines that are treated with 265 drugs. Of course there are many NAs, i.e., not all drugs are tested per cell lines. 

## Data Selection and Save
First we select Xs for which we have all measurements:
```{r , warning = FALSE}
cls = muths$cls & CN$cls & Expr$cls & RPPA$cls
```

We can separate things related to each drug and save them or we can save the cls selector and use it along with X and Y. 
```{r , warning = FALSE}
y <- Sanger$dat
save(cls, x, y, file = file.path(paths$clean, 'ccle.Rda'))
```

Let's check number of cell line per drug:

```{r , warning = FALSE}
hist(apply(Sanger$dat, 2, function(x) sum(!is.na(x))))
```

We need to filter out cell lines for which we do not have the response measured. Let's have an example:

```{r , warning = FALSE}
y = y[, 'Ponatinib']
cls = cls & (!is.na(y))
x = x[which(cls), ]
x2 = x2[which(cls), ]
y = y[which(cls)]
dim(x)
dim(x2)
length(cls)
length(y)
```

# Elastic Net Prediction

## Pre-Processing
After selection of the relevant subset of data, we need to remove the features that have neare zero variation. 
```{r , warning = FALSE}
library(caret)
zero_var_cols <- nearZeroVar(x, saveMetrics = F)
length(zero_var_cols)
X_clean <-  x[,-zero_var_cols]

zero_var_cols <- nearZeroVar(x2, saveMetrics = F)
length(zero_var_cols)
X_clean2 <- x2[,-zero_var_cols]
dim(x)
dim(x2)
dim(X_clean)
dim(X_clean2)
```

Below code is a part of their pre-processing which is wrong. They compute the correlation of the input and output for filtering features outside of the cross validation. But let's do this the wrong way to fully replicate their results:
```{r , warning = FALSE}
cc = cor(X_clean, y, use = 'p')
x = X_clean[,which(abs(cc)>0.1)]
dim(x)

cc = cor(X_clean2, y, use = 'p')
x2 = X_clean2[,which(abs(cc)>0.1)]
dim(x2)
```

Finally, a little bit more clean up and normalization:

```{r , warning = FALSE}
x = as.numeric.matrix(x)
x2 = as.numeric.matrix(x2)
  
x[which(is.na(x))]=0; #TP53_mut.hsrec
x2[which(is.na(x2))]=0; 
  
ii = which(duplicated(t(x)))
x = x[,-ii]
ii = which(duplicated(t(x2)))
x2 = x2[,-ii]
  
x2 = scale(x2)
x = scale(x)
```

## Prediction

They super optimize glmnet by also searching for `alphas= seq(.2,1,.1)`, i.e., for each alpha they do 10 fold cross-validation to find the best lambda. Even for one drug, this is going to take a while. I don't know why, but they even do the cross validation `nTimes=10`. This means that for each combination of `alpha` (given by the user) and `lambda` (determined by glmnet) they do ten fold cross validation and find the average error, but to be sure about this average error they redo the fold assignment (data shuffling), if CV.glmnet picks another labmda, they redo it using the first lambda. This is a lazy way of doing grid search: they don't want to manually figure out a good range and breaks for the lambda, but at the same time, they don't want to have different lambda every time. 

```{r , warning = FALSE}
## find optimal alpha for glmnet 
glmTuneAlpha = function (x, y, alphas= seq(.2,1,.1), K=10, ntimes=10, rseed=NULL){  # runs repeated CV to find optimum values of alpha and lambda 
  if (!is.null(rseed)){
    set.seed(rseed)
  }
  lambdas = vector("list", length(alphas))  #lambdas used 
  serr = vector("list", length(alphas))  #sum of errors
  for(i in 1:ntimes){
    foldid = createFolds(1:length(y), k =K, list=FALSE); # cross validation fold IDs
    for(j in 1:length(alphas)){
      print(paste(i,alphas[j]))
      cvfit = cv.glmnet(x, y, foldid=foldid, alpha=alphas[j])   
      if (i==1){
        serr[[j]] = matrix(nrow = ntimes, ncol= length(cvfit$cvm ))
        serr[[j]][i,] = cvfit$cvm
        lambdas[[j]] = cvfit$lambda
      }else{
        if (sum(lambdas[[j]]!=cvfit$lambda)>0){ # this should not happen if glmnet always pick same lambdas
          print(paste("-- lambdas did not match -- recalculating", i, j))
          cvfit = cv.glmnet(x, y, foldid=foldid, alpha=alphas[j], lambda =lambdas[[j]])   
        }            
        serr[[j]][i,] = cvfit$cvm
      }        
    }
  }
  cverr = serr 
  
  for(i in 1:length(alphas)){
    serr[[i]]= colMeans(cverr[[i]])
  }
  jmin = order(unlist(lapply(serr, min)))[1]
  imin = order(serr[[jmin]])[1]
  alpha.min = alphas[jmin]
  lambda.min = lambdas[[jmin]][imin]
  mcverr.min = min(serr[[jmin]])
  return (list(alphas=alphas, lambdas=lambdas, cverr=cverr, rseed=rseed, alpha.min=alpha.min, lambda.min=lambda.min, mcverr.min=mcverr.min))
}

bootstrap=function(x,y, alpha, lambda, n=100 ){
  N = length(y); 
  cnt = rep(0, ncol(x)); names(cnt)=colnames(x)
  sx = cnt; 
  sx2 = cnt; 
  
  for(bsi in 1:n){
    ii = sample(N, N, replace =TRUE)
    fit = glmnet(x[ii,], y[ii], alpha = alpha)
    cc = coef(fit,  s = lambda )
    w=as.matrix(cc)[-1]
    jj = which(abs(w)>0)
    cnt[jj] = cnt[jj]+1;
    sx = sx+w; 
    sx2= sx2+w^2
    print(bsi)
  }
  sx = sx/n; 
  sx2 = sx2/n - sx^2
  cnt = cnt/n
  return(list(cnt = cnt, w = sx, sw = sx2))
}      
```

## Visualization
Some hidden code fore visualizaiton. 
```{r , warning = FALSE, echo=FALSE}
## plot error curves 
plotErrCurves = function(tp,tp1, dnam, lab0, lab1){
  rep.par <- par(no.readonly=TRUE)
  
  layout(matrix(c(rep(0,4),1,rep(0,4)),3,3,byrow=TRUE), heights=c(2.5,5,2.5), widths=c(2,6,2))
  
  i = grep(tp$alpha.min, tp$alphas)
  cverr = tp$cverr[[i]]
  merr = apply(cverr, 2, mean)
  msd = apply(cverr, 2, sd)
  lambdas = tp$lambdas[[i]]
  
  cverr1 = tp1$cverr[[i]]
  merr1 = apply(cverr1, 2, mean)
  msd1 = apply(cverr1, 2, sd)
  lambdas1 = tp1$lambdas[[i]]
  
  plot(lambdas, merr, col='blue', ylim = range(merr,merr1), log='x', main=paste(dnam,'\n',expression(alpha),'=', tp$alpha.min), xlab = expression(lambda), ylab='CV error')
  for(i in 1:length(merr)) { lines(rep(lambdas[i],2),merr[i]+msd[i]*c(-1,1), col=adjustcolor('blue', alpha.f = 0.2))}
  points(lambdas1, merr1, col='red')  
  for(i in 1:length(merr1)) { lines(rep(lambdas1[i],2),merr1[i]+msd1[i]*c(-1,1), col=adjustcolor('red', alpha.f = 0.2))}
  
  legend('topleft', c(lab0, lab1), col=c('blue','red'), pch=20,bty='n', cex=0.9)
  
  i = grep(tp1$alpha.min, tp1$alphas)
  cverr = tp$cverr[[i]]
  merr = apply(cverr, 2, mean)
  msd = apply(cverr, 2, sd)
  lambdas = tp$lambdas[[i]]
  
  cverr1 = tp1$cverr[[i]]
  merr1 = apply(cverr1, 2, mean)
  msd1 = apply(cverr1, 2, sd)
  lambdas1 = tp1$lambdas[[i]]
  
  plot(lambdas, merr, col='blue', ylim = range(merr,merr1), log='x', main=paste(dnam,'\n',expression(alpha),'=',tp1$alpha.min), xlab = expression(lambda), ylab='CV error')
  for(i in 1:length(merr)) { lines(rep(lambdas[i],2),merr[i]+msd[i]*c(-1,1), col=adjustcolor('blue', alpha.f = 0.2))}
  points(lambdas1, merr1, col='red')  
  for(i in 1:length(merr1)) { lines(rep(lambdas1[i],2),merr1[i]+msd1[i]*c(-1,1), col=adjustcolor('red', alpha.f = 0.2))}
  
  legend('topleft', c(lab0, lab1), col=c('blue','red'), pch=20,bty='n', cex=0.9)
  par(rep.par)
}   

colfunc <- function(n) {
  library(RColorBrewer)
  hmcol <- colorRampPalette(brewer.pal(10, "RdBu"))(n)
  return(hmcol)
}

redblue.colors = colfunc; # not original 

#drawHeatmap originally developed by Nicolas Stransky
drawHeatmap <- function(beta, featureData, responseData, featureNames=NULL, featurePerc=NULL,
                        responseLabel="", suffix, outputdir="", heatmapThresh=3, writeTable=FALSE, responseName="sensitivity", cex.factor=1) {
  features <- rownames(beta)
  if (!is.null(featureNames)){
    hmLabels <- featureNames
  }else{
    hmLabels <- features
  }
  
  if (is.null(featurePerc)) {
    barplotcol <- "darkblue"
  } else {
    ## barplotcol <- as.character(lapply(as.numeric(featurePerc)^2, function(n) rgb(0,0,139,(n*256), maxColorValue=256)))
    barplotcol <- unlist(lapply(as.numeric(featurePerc)^2, function(x) rgb(colorRamp(c("#E2E2F2", "darkblue"))(x), maxColorValue=255)))
  }
  
  cl <- intersect(colnames(responseData), colnames(featureData))
  clustmatrix <- data.matrix(featureData[features,
                                         colnames(responseData)[order(responseData[,cl])], drop=FALSE])
  ## Write data table
  if (writeTable) write.csv(clustmatrix, file=paste(outputdir,"/Enet_",make.names(rownames(responseData)),"_finalModel", suffix, "_matrix",NROW(clustmatrix),".csv",sep=""))
  
  clustmatrix[clustmatrix < -heatmapThresh] <- -heatmapThresh
  clustmatrix[clustmatrix >  heatmapThresh] <-  heatmapThresh
  
  rep.par <- par(no.readonly=TRUE)
  layout(matrix(c(1,2,0,3),2,2,byrow=TRUE), heights=c(8,1), widths=c(1,5))
  ## weights barplot
  par(mar=c(2.5,.5,3.8,0), las=1, mgp=c(3,.6,0))
  plot(0, xlim=c(min(as.numeric(beta), 0), max(as.numeric(beta), 0)), xlab="", ylab="", type="n", axes=FALSE)
  par(usr=c(par("usr")[1:2], 0, length(as.numeric(beta))))
  abline(h=c(0,length(as.numeric(beta))), col="grey", lty=2, lwd=1)
  barplot(as.numeric(beta), add=TRUE, horiz=TRUE, border=NA, col=barplotcol, space=0, cex.axis=.7, axes=FALSE)
  axis(1, cex.axis=.7, line=.6)
  mtext("weights", 1, line=2.2)
  ## heatmap
  par(mar=c(2.5,2,3.8,11), las=1)
  image(1:ncol(clustmatrix), 1:nrow(clustmatrix), t(clustmatrix), col=rev(redblue.colors(200)), ylab="", xlab="",
        zlim=c(-heatmapThresh,heatmapThresh), main=rownames(responseData), yaxt="n", cex.main=1.5*cex.factor)
  if(nrow(clustmatrix)<4) par(yaxp=c(1, nrow(clustmatrix), max(nrow(clustmatrix)-1, 1)))
  axis(2)
  axis(4,
       at=1:length(features), labels=hmLabels,
       cex.axis=cex.factor*min((ifelse(length(features)<=130, .2, 0) + .7/log10(length(features))), 1.3),
       tick=FALSE)
  par(xpd=TRUE)
  segments(-100, par("usr")[3], par("usr")[1], col="grey", lty=2, lwd=1) ; segments(-100, par("usr")[4], par("usr")[1], col="grey", lty=2, lwd=1)
  segments(par("usr")[1], par("usr")[3], par("usr")[1], -100, col="grey", lty=2, lwd=1) ; segments(par("usr")[2], par("usr")[3], par("usr")[2], -100, col="grey", lty=2, lwd=1)
  ## colorscale
  par(new=TRUE, mar=c(52,46,.5,.5), mgp=c(3,.7,0), xpd=FALSE)
  if (TRUE){
    image(seq(-heatmapThresh,heatmapThresh,length.out=200),1, as.matrix(seq(-heatmapThresh,heatmapThresh,length.out=200)),
          col=rev(redblue.colors(200)), axes=FALSE, xlab="", ylab="")
  }
  axis(1, cex.axis=.7)
  ## Response scatter plot
  par(mar=c(3,2,0,11), las=1, mgp=c(3,2,1), las=2, xpd=TRUE)
  plot(responseData[,order(responseData[,cl])], xlab="", ylab="", 
       xaxt="n", yaxt="n", cex.axis=.5, type="n", frame.plot=FALSE)
  par(usr=c(0, length(cl), par("usr")[3:4]), xpd=TRUE)
  responseVals <- responseData[,order(responseData[,cl])]
  abline(v=c(0, length(cl)), col="grey", lty=2, lwd=1)
  points((1:length(cl))-.5, responseVals, cex=.9, pch=20)
  axis(4)
  axis(4, at=mean(range(responseData[,cl])), labels=paste(rownames(responseData), responseName, sep="\n"), tick=FALSE, line=1.8, cex.axis=1.2*cex.factor)
  par(rep.par)
}
```

## Experiment

```{r , warning = FALSE}
library(e1071) 
library(glmnet)

names(y)=rownames(x)

dnam='Ponatinib'
x1=x2
rseed=10
alphas = seq(.2,1,.1)
K=10 #3; #10; 
ntimes=10 #2; 10;
tunePars = glmTuneAlpha(x,y, alphas= alphas, K=K, ntimes=ntimes, rseed=rseed)
tunePars1 = glmTuneAlpha(x1,y, alphas= alphas, K=K, ntimes=ntimes, rseed=rseed)

bs = bootstrap(x,y, alpha=tunePars$alpha.min, lambda=tunePars$lambda.min, n=200 );
bs1 = bootstrap(x1,y, alpha=tunePars1$alpha.min, lambda=tunePars1$lambda.min, n=200 );

outfn=file.path(paths$scratch, paste('FigS11hi_Ponatinib_enet_res', '.RDAT', sep=''));
save(tunePars,tunePars1,bs,bs1, file =outfn)

pdf(paste(outfn,'.pdf', sep=''), width=10, height=10)

plotErrCurves(tunePars1,tunePars,dnam, 'w/o RPPA', 'w RPPA');

jj = order(-bs$cnt)[1:15]
beta =matrix(bs$w[jj], ncol=1); rownames(beta)=names(bs$w[jj])
featureData=t(x[,jj])
responseData = matrix(y, nrow=1);  colnames(responseData)=names(y); rownames(responseData)=dnam
featurePerc=matrix(bs$cnt[jj],ncol=1);  rownames(featurePerc)=names(bs$w[jj])
featureNames= paste(rownames(featurePerc), " (", signif(featurePerc,2), ")", sep="")
responseLabel=paste(dnam,'activity area', sep='\n')

drawHeatmap(beta, featureData, responseData, featureNames, featurePerc,
            responseLabel, suffix='', outputdir="", heatmapThresh=3, writeTable=FALSE, responseName="Activity area", cex.factor=1);

jj = order(-bs1$cnt)[1:15]
beta =matrix(bs1$w[jj], ncol=1); rownames(beta)=names(bs1$w[jj])
featureData=t(x1[,jj])
responseData = matrix(y, nrow=1);  colnames(responseData)=names(y); rownames(responseData)=dnam
featurePerc=matrix(bs1$cnt[jj],ncol=1);  rownames(featurePerc)=names(bs1$w[jj])
featureNames= paste(rownames(featurePerc), " (", signif(featurePerc,2), ")", sep="")
responseLabel=paste(dnam,'activity area', sep='\n')

drawHeatmap(beta, featureData, responseData, featureNames, featurePerc,
            responseLabel, suffix='', outputdir="", heatmapThresh=3, writeTable=FALSE, responseName="Activity area", cex.factor=1);

dev.off()
```

# Appendix: Session Info
This anlysis was performed in this environment:
```{r si}
sessionInfo()
```